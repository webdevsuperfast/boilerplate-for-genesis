# Boilerplate for Genesis

Version: 1.5

## Author:

SuperFastBusiness

## Summary

Boilerplate for Genesis is a starter theme for the Genesis Framework. It uses SCSS, Compass and Gulp for all processing tasks. Tested up to WordPress 4.2 and Genesis 2.1.2.

### Deployment

* Clone repository
* Install dependencies
    * [Ruby](http://rubyinstaller.org/)
    * [Node.js](https://nodejs.org/)
    * [Compass](http://compass-style.org)
    * ~~[Breakpoint](http://breakpoint-sass.com/)~~
* Open Command Line and run `npm install` to install gulp and dependencies.
* Run `bower install` to install packages.
* Run `gulp` to compile scripts and stylesheet.

### Installation

* Clone/download files from Bitbucket repository.
* Copy/extract/upload to WordPress themes folder.
* If you don't want to use Gulp replace `style.css` with `temp/css/style.css` and `assets\js\app.min.js` with `temp/js/app.js` to use the uncompressed files.

### Features

1. Compass, SCSS and easy-to-use of mixins ready to go
2. Easy to customize
3. Gulp
4. Child theme tweaks
    * WordPress `head` cleanup
    * Before Footer Hook `mb_before_footer`
    * Before Footer Widget Area Hook `mb_before_footer_widget_area`
    * Additional widget areas
    * Many more...
5. ~~Built-in SuperCPT support~~
6. Built-in Vafpress Theme Options Framework support
7. Built-in TGM Plugin Activation support
8. ~~Built-in Pushy Off-Canvas Navigation support~~
9. [WP-LESS](https://github.com/sanchothefat/wp-less) support
10. Theme Updater using [GitHub Updater](https://github.com/afragen/github-updater)
11. [AniJS](http://anijs.github.io/) support
12. [Font Awesome](https://fortawesome.github.io/) support
13. Built-in [FitVids](http://fitvidsjs.com/) support for responsive videos.
14. CSS animations using [Animate.css](https://daneden.github.io/animate.css/)
15. ~~Hover animations using [Hover.css](http://ianlunn.github.io/Hover/)~~

### Suggested Plugins

The plugins below are already added into the [TGM Plugin Activation](http://tgmpluginactivation.com/) script. Just install them after activating the theme.

* [Page Builder by SiteOrigin](https://wordpress.org/plugins/siteorigin-panels/)
* [SiteOrigin Widgets Bundle](https://wordpress.org/plugins/so-widgets-bundle/)
* ~~[SuperCPT](https://wordpress.org/plugins/super-cpt/)~~
* ~~[Vafpress](https://github.com/vafour/vafpress-framework-plugin)~~
* [GitHub Updater](https://github.com/afragen/github-updater)
* [Developer Bundle](https://bitbucket.org/webdevsuperfast/developer-bundle)

### Update

The theme layout grid now uses [CSS Tricks](https://css-tricks.com/dont-overthink-it-grids/) awesome grid implementation.

### Credits

Without these projects, this WordPress Genesis Starter Child Theme wouldn't be where it is today.

* [Genesis Framework](http://my.studiopress.com/themes/genesis/)
* [SASS / SCSS](http://sass-lang.com/)
* [Less](http://lesscss.org/)
* [Compass](http://compass-style.org)
* [Gulp](http://gulpjs.com/)
* [Bower](https://github.com/bower/bower)
* ~~[Page Builder by SiteOrigin](https://wordpress.org/plugins/siteorigin-panels/)~~
* ~~[SiteOrigin Widgets Bundle](https://wordpress.org/plugins/so-widgets-bundle/)~~
* ~~[SuperCPT](https://wordpress.org/plugins/super-cpt/)~~
* ~~[Vafpress](https://github.com/vafour/vafpress-framework-plugin)~~
* ~~[Pushy](https://github.com/christophery/pushy)~~
* [WP-Less](https://github.com/sanchothefat/wp-less)
* [TGM Plugin Activation](http://tgmpluginactivation.com/)
* [Font Awesome](https://fortawesome.github.io/Font-Awesome/)
* [Animate.css](https://daneden.github.io/animate.css/)
* [FitVids](http://fitvidsjs.com/)
* [AniJS](http://anijs.github.io/)
* ~~[Hover.css](http://ianlunn.github.io/Hover/)~~
* [CSS Tricks](https://css-tricks.com)
